var express    = require('express'),
    app        = express(),
    mongodb = require('./db'),
    bodyParser = require('body-parser'),
    port = process.env.PORT || 8080,
    Globals = require('./globals'),
    log4js = require('log4js');
    
    

log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');

app.use(function(req,res,next){
  
  res.header('Access-Control-Allow-Origin', '*'); // We can access from anywhere
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
    next();
});

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(logResponseBody);




function logResponseBody(req, res, next) {
    var oldWrite = res.write,
        oldEnd = res.end,
        chunks = [],
        t1 = new Date();

    res.write = function (chunk) {
        chunks.push(chunk);
        oldWrite.apply(res, arguments);
    };

    res.end = function (chunk) {
        if (chunk)
            chunks.push(chunk);

//        var body = Buffer.concat(chunks).toString('utf8');
        var t2 = new Date();
//        logger.trace((t2 - t1) + " : Path: " + req.path + " :Req.body:::: " + JSON.stringify(req.body) + " : ResponseBody:::: "+ body);

        oldEnd.apply(res, arguments);
    };

    next();
};


process.on('SIGINT', function() {
    mongodb.close(function(){
        logger.info('closing db');
        process.exit(0);
    });
});

process.on('uncaughtException', function(err) {
    // handle the error safely
    logger.info(err.stack);
});

mongodb.connect(Globals.MongoHost, Globals.MongoPort, Globals.MongoDB, function(err){
    if(err){
        logger.info("Problem in connecting MongoDB.");
    }else{
        logger.info("Connected to MongoDB.");
        app.listen(port, function () {
            logger.info('API\'s work at http://localhost:' + port + " url.");
        });
    }
});

app.get('/', function(req, res) {
    res.sendFile(Globals.appRoot + '/public/views/layout.html');
});


// dash board  apis 

 
  // filing the  data into the  dash baord 

   app.get('/userslistcount',function(req,res){
   

     mongodb.findAll("devicelist",function(err,result){

        if(err)
         {
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{

             res.json(result.length); 

         }

     });
   
   
   });



app.get('/usersActivelistcount',function(req,res){
   
   
   mongodb.findByObjects("devicelist",{shsstatus:"Active"},function(err,result)
     {
       
        if(err)
         {
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{

             res.json(result.length); 

         }
             
       

      });
   
});

app.get('/usersInactivelistcount',function(req,res){
   
  mongodb.findByObjects("devicelist",{shsstatus:"InActive"},function(err,result)
     {
       
        if(err)
         {
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{

             res.json(result.length); 

         }
             
       

      });
   
});


app.get('/userslist',function(req,res){
   

     mongodb.findAll("devicelist",function(err,result){

        if(err)
         {
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{

             res.json(result); 

         }

     });
   
   
   });




// Device  dashboard 

// getting the device  list 

app.get('/adddevice',function(req,res){
    
    
     mongodb.findAll("devicelist",function(err,result){

        if(err)
         {
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{

              //console.log(""+JSON.stringify(result));
             res.json(result); 
            
         }

     });
   
    
});

// removing the  devices from the list 

app.delete('/deletedevice/:id',function(req,res){
    var id=req.params.id;
  
    mongodb.deleteOneByID("devicelist",id,function(err,result){

        if(err)
         {
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{   
             res.json(result);    
         }

     });
});

// editing the devices for the list 

app.get('/devicelistdata/:id',function(req,res)
{
    var id=req.params.id;
    

   // logger.error("app js id"+id);

    mongodb.findById("devicelist",id,function(err,result){

        if(err)
         {
            //console.log("err"+err);
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{   
         // console.log(""+JSON.stringify(result));
             res.json(result);    
         }

     });



});





// adding the device 

app.post('/adddevice',function(req,res){
    
    //console.log(""+JSON.stringify(req.body));
      
    
    mongodb.save('devicelist',req.body, function(err, result)
    {
            if(err)
            {

                res.send({"status":0,"message":"error in insertion of the record"});
            }
            else
            {

               res.json(result); 
            }

            
      }  );
    
});


// payments dashboard 


// adding data to  payments table  by the sms 

app.post('/addpayment',function(req,res){

    var idofscrchcard;
    var deviceid;
    var scratchperiod;
    var scratchcost;
    var payid;
    var payactivationdate= new Date();
    var amountpayed;

   // logger.info("present date"+payactivationdate);

    var paydeactivationday;
     var scartchcardno = req.body.scartchcardno;
     logger.error("scratch card "+scartchcardno);


     mongodb.findByObjects("devicelist",{"shsnumber":parseInt(req.body.shsnumber)},function(err,shsdata){
         

           if(err)
           {
               res.send({"status":0,"message":"error in retrivien data from devic list"});
           }
           else
           {
               if(shsdata.length > 0)
               {
                  amountpayed=shsdata[0].shsamtpayed;
                  
                   mongodb.findByObjects("Scratchcard",{"scartchcardno":req.body.scartchcardno},function(err,result){

        if(err)
           {
              res.send({"status":0,"message":"error in retriving  of the scratch card record"});
           }
           else
           {
                 if(result.length>0)
                 {
                     idofscrchcard=result[0]._id;
                     scratchperiod=result[0].cardduration;
                     scratchcost=result[0].cost;
                     paydeactivationday=new Date().addDays(parseInt(scratchperiod));

                     deviceid=req.body.shsnumber;
                     //logger.info("deactivation date"+paydeactivationday);

                     logger.error("shs value in second time "+result[0].shsnumber);
                     logger.error("scno value in second time "+result[0].scartchcardno);
                   
                       if(result[0].shsnumber === "null")
                       {


                            mongodb.findByObjects('payments',{"shsnumber":req.body.shsnumber}, function(err, result1){
                             
                             if(err)
                                {
                                    res.send({"status":0,"message":"error in retriving of the record from payment"});
                                }
                                else
                                {
                                        if(result1.length ==0)
                                        {
                                          //Doc: first time entry
                                            var payobject={
                        
                                                               "shsnumber":req.body.shsnumber,
                                                               "scartchcardno":req.body.scartchcardno,
                                                               "phonenumber":req.body.phonenumber,
                                                               "payamount":scratchcost,
                                                               "paydate":payactivationdate

                                                           };

                                                 mongodb.save('payments',payobject, function(err, result){

                                                        if(err)
                                                          {

                                                           res.send({"status":0,"message":"error in insertion of the record"});
                                                           }
                                                         else{

                                                                var obj={"shsnumber":req.body.shsnumber};
                                                                mongodb.update("Scratchcard", idofscrchcard, obj, function(err, _data){
                                                                 
                                                                   if(err){
                            
                                                                     res.send({"status": 0, "message": "some problem in saving details: " + err});
                                                                    }                                           
                                                                    else
                                                                    {
                                                                       amountpayed+=parseInt(scratchcost);
                                                                       logger.error("cost updating"+amountpayed);
                                                                       var deviceobj={
                                                                                   "shsstatus":"Active",
                                                                                   "shsactivationdate":payactivationdate,
                                                                                   "shsdeactivationdate":paydeactivationday,
                                                                                    "shsamtpayed":amountpayed,
                                                                                    "shslastpaydate":new Date()


                                                                                 };
                                                                         mongodb.updateByQuery("devicelist",{"shsnumber":parseInt(deviceid)},deviceobj,function(err,d){

                                                                              if(err)
                                                                              {
                                                                                  res.send({"status":0,"message":"problem in updatating the record"});
                                                                              }
                                                                              else
                                                                              {
                                                                                  //res.json(d);
                                                                                 // res.send({"status":1,"message":"scratchcard card activated succefully to your device"});
                                                                              
                                                                                mongodb.save("paymentshistory",payobject,function(err,phdata){

                                                                                    logger.error("added succefully to pay history");

                                                                                     res.send({"status":1,"message":"scratchcard card activated succefully to your device ,please restart your solar home system and start using it"});

                                                                                  });
                                                                                 

                                                                              }

                                                                           });





                                                                    }




                                                                });

                                                           }  
 



                                                 });              




                                        }
                                        else
                                        {
                                          //Doc: second time entry
                                         

                                         var histobj={
                       
                                                "paymentid":result1[0]._id,
                                                "shsnumber":result1[0].shsnumber,
                                                "scartchcardno":result1[0].scartchcardno,
                                                "phonenumber":result1[0].phonenumber,
                                                "payamount":result1[0].payamount,
                                                "paydate":result1[0].paydate

                                            }
                                       
                                       if(result[0].shsnumber === "null")
                                       {
                                           //Doc: first time enrty in history 
                                          mongodb.save("paymentshistory",histobj,function(err,hdata){
                                              if(err)
                                              {
                                                   res.send({"status":0,"message":"problem in inseting the record into history"});
                                              }
                                              else
                                              {
                                                 var idtoupdate=hdata._id;
                                                 amountpayed+=parseInt(scratchcost);
                                                 logger.error("cost updating in pay history"+amountpayed);
                             
                                                   var deviceobj={
                                                       "shsstatus":"Active",
                                                       "shsactivationdate":payactivationdate,
                                                       "shsdeactivationdate":paydeactivationday,
                                                       "paymentid":idtoupdate,
                                                       "shsamtpayed":amountpayed,
                                                        "shslastpaydate":new Date()
                                                     };


                                                       var payobject={
                                                  
                                                  // "shsnumber":req.body.shsnumber,
                                                   "scartchcardno":req.body.scartchcardno,
                                                   "phonenumber":req.body.phonenumber,
                                                   "payamount":scratchcost,
                                                   "paydate":payactivationdate,
                                                   //"_id":idtoupdate

                                                   };

                                                   mongodb.updateByQuery("payments",{"shsnumber":req.body.shsnumber},payobject,function(err,pdata)
                                                   {
                                                        if(err)
                                                            {
                                                               res.send({"status":0,"message":"problem in update the record into payments"});
                                                            }
                                                         else
                                                           {

                                                              mongodb.updateByQuery("devicelist",{"shsnumber":parseInt(req.body.shsnumber)},deviceobj,function(err,ddata)
                                                              {
                                                                 if(err)
                                                                  {
                                                                       res.send({"status":0,"message":"problem in update the record into devicelist"});
                                                                  }
                                                                  else
                                                                  {
                                                                    var obj={"shsnumber":parseInt(req.body.shsnumber)};
                                                                     mongodb.update("Scratchcard",idofscrchcard,obj,function(err,ldata){

                                                                      if(err)
                                                                      {
                                                                       res.send({"status":0,"message":"problem in update the record into scratchcard"});
                                                                      }
                                                                      else
                                                                      {
                                                                        res.send({"status":1,"message":"scratchcard card activated succefully to your device ,please restart your solar home system and start using it"});
                                                                      }

                                                                     });

                                                                      


                                                                  }







                                                              });

                                                           }   



                                                   });



             


                                              }

                                          });
                                       }

                                       else
                                       {
                                          res.send({"status":0,"message":"scratchcard already used"});

                                       }





                                        }

                                }

 
                            });
          




                       }
                       else
                       {
                         res.send({"status":0,"message":"scratchcard already used"});
                       }

                  

                 }
                 else
                 {
                 
                 res.send({"status":0,"message":"This is not a valid scartchcardno"});
                 }
           }



    });

                  
               }
               else
               {
                  res.send({"status":0,"message":"shsnumber doesn't exist ,please try with a valid shsnumber"});
               }
              
           }       
    

     });

  
});









// get all the payments

app.get('/addpayment',function(req,res){
    
    
     mongodb.findAll("payments",function(err,result){

        if(err)
         {
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{

              //console.log(""+JSON.stringify(result));
             res.json(result); 
            
         }

     });
   
    
});





// get all payments history table 


app.get('/addpaymenthistory/:id',function(req,res){
    
    
    var param=req.params.id;
 
     mongodb.findByObjects("paymentshistory",{"shsnumber":param},function(err,result)
        {

           if(err)
           {

            res.send({"status":0,"message":"error in retriving of the record"});
           }
            else
            {

             res.json(result); 
            }


        });
   
    
});


// api  for device level

app.post('/getEvent',function(req,res){
    
    //console.log(""+JSON.stringify(req.body));

    
    mongodb.findByObject('devicelist',{"shsnumber":parseInt(req.body.shsnumber)},function(err,edata)
     {

         if(err)
         {
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{

               var status=edata.shsstatus;
               var adate=edata.shsactivationdate;
               var ddate=edata.shsdeactivationdate;
 
              var resobj={
              
                   "shsstatus":status,
                   "shsactivationdate":adate,
                   "shsdeactivationdate":ddate

             };

             res.json(resobj); 

         }


     } );
   


});

app.post('/setStatus',function(req,res){

   // console.log(""+JSON.stringify(req.body));
      
      // update the latest values  to the device list and add to the setstaatus 

     var objc={

           "shslatitude":req.body.shslatitude,
           "shslongitude":req.body.shslongitude,
           "shslightvalue":req.body.shslightvalue,
           "shschargingstatus":req.body.shschargingstatus,
           "shschargingvalue":req.body.shschargingvalue,
           "shsstatusdate":new Date()

     };
     
 
     mongodb.updateByQuery("devicelist",{"shsnumber":parseInt(req.body.shsnumber)},objc,function(err,ldata){

       if(err)
       {
         res.send({ "status":0,"message":"error in insertion of the record"});
       }
       else{


        mongodb.save('devicestatus',req.body, function(err, result)
    {
            if(err)
            {

                res.send({"status":0,"message":"error in insertion of the status record"});
            }
            else
            {
               //res.json(result); 
               res.send({"status":1,"message":"updated status succesfully"})
            }

            
      }  );


       }


     });

    
    
});






// storing the scratch card details 

app.post('/Scratchcard',function(req,res){
    
    //console.log(""+JSON.stringify(req.body));
      
    
    mongodb.save('Scratchcard',req.body, function(err, result)
    {
            if(err)
            {

                res.send({"status":0,"message":"error in insertion of the record"});
            }
            else
            {

               res.json(result); 
            }

            
      }  );
    
});


  // chnging the status of the device
  app.post('/updatestatus',function(req,res){

      updateobj={
        
          "shsstatus":req.body.shsstatus

      };

     mongodb.updateByQuery("devicelist",{"shsnumber":parseInt(req.body.shsnumber)},updateobj,function(err,updata){
 
       if(err)
       {
              res.send({"status":0,"message":"error in retriving the record"});
       }
       else
       { 
           //res.json(updata);
           res.send({"status":1,"message":"status updated succesfully"});
       }


     });

  });


 
 Date.prototype.addDays = function(days) {
 var dat = new Date(this.valueOf());
 dat.setDate(dat.getDate() + days);
 return dat;
}



// get all the scratch cards

app.get('/getscratchcards',function(req,res){
    
    
     mongodb.findAll("Scratchcard",function(err,result){

        if(err)
         {
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{

              //console.log(""+JSON.stringify(result));
             res.json(result); 
            
         }

     });
   
    
});


 
















