var App = angular.module('myApp',['ngRoute']);
App.config(['$routeProvider', function($routeProvider){
    $routeProvider
        .when('/login', {
            templateUrl: '/views/login.html',
            controller: 'loginCtrl'
        })
        .when('/dashboard', {
            templateUrl: '/views/customerCareDashboard.html',
            controller: 'dashBoardCtrl'
        })
        .when('/tickets', {
            templateUrl: '/views/tickets.html',
            controller: 'ticketCtrl'
        })
        .when('/comment', {
            templateUrl: '/views/customerCareComment.html',
            controller: 'commentCtrl'
        })
        .when('/userInfo', {
            templateUrl: '/views/customerCareUserData.html',
            controller: 'userDataCtrl'
        })
        .when('/deviceInfo', {
            templateUrl: '/views/customerCareDeviceData.html',
            controller: 'deviceInfoCtrl'
        })
        .otherwise({
            redirectTo: 'login'
        });
}]);
App.factory('_', ['$window', function($window) {
  return $window._; // assumes underscore has already been loaded on the page
}]);
App.run(function($rootScope) {
    $rootScope.userSelect = false;
    $rootScope.userData = {};
    $rootScope.ticketData = {};
    $rootScope.baseUrl = 'localhost:3000/';
    // $rootScope.baseUrl = 'http://127.0.0.1:8080/';
    // $rootScope.baseUrl = 'http://192.168.2.166:8080/';
    $rootScope.headerTitle = "";
    $rootScope.customerData = {};
});
App.controller('mainCtrl', function($rootScope, $scope, $filter, $http, $interval, $timeout) {
    $timeout(function() {
        $scope.headerTitle =  $rootScope.headerTitle;
    }, 100);
    $scope.getTime = function(){
        var offTime = $filter('date')(new Date(), 'HH:mm:ss').split(':');
        var text = " AM";
        if (offTime[0] > 11){
            text = " PM";
        }
        offTime[0] = ("00" + (offTime[0] % 12).toString()).slice(-2);
        if (offTime[0] == "00"){
            offTime[0] = "12";
        }
        $scope.currentTime = offTime.join(':') + text;
        $scope.date = $filter('date')(new Date(), 'MMMM dd,  yyyy');
        $scope.requestBtn = $rootScope.requestBtn;
        $scope.userSelect = $rootScope.userSelect;
        $scope.options[0].label = $rootScope.userData.user_id;
    };
    $scope.options = [
        { label: 'John Doe', value: 1 },
        { label: 'LogOut', value: 2 }
    ];
    $scope.logoutSelect = $scope.options[0];
    $scope.getTime();
    $interval($scope.getTime, 1000);
    $scope.logout = function(_this){
        $scope.userSelect = $rootScope.userSelect = false;
        if($scope.logoutSelect.value === $scope.options[1].value){
            $scope.logoutSelect = $scope.options[0];
            $scope.requestBtn = $rootScope.requestBtn = false;
            window.location = "#/";
        }
    };
    $scope.showRequests = function(){
        window.location = '#/tickets';
    };
});
App.controller('loginCtrl', function($rootScope, $scope, $http) {
    $rootScope.headerTitle = "Customer Support Portal";
    var baseUrl = $rootScope.baseUrl;
    $scope.postRequest = function(url, data){
        $http.post(baseUrl+url, data).success( function(response) {
            delete $scope.credentials;
            if (response.status === 1){
                window.location = "#/dashboard";
            }else{
                alert(response.message);
            }
        }).error( function(){
            console.log('error');
        });
    };
    $scope.Login = function(){
        var keys = ['userId', 'password'];
        for(var i in keys){
            if($scope.credentials[keys[i]] == undefined || $scope.credentials[keys[i]] == ""){
                alert("all fields mandatory");
                return false;
            }
        }
        var data = {user_id: $scope.credentials.userId, password: $scope.credentials.password};
        $rootScope.userData = data;
        $scope.postRequest('customerCare/login', data);
    };
});
App.controller('dashBoardCtrl', function($rootScope, $scope, $http) {
    $rootScope.userSelect = $rootScope.requestBtn = true;
    var baseUrl = $rootScope.baseUrl;
    $scope.credentials = {};
    $scope.getCustomerDetails = function(data){
        if(data){
            $rootScope.ticketData = data;
            $scope.email = data.email;
            $scope.IsHidden = true;
            $scope.credentials.ticketId = data.ticketId;
        }else{
            postRequest('customerCare/getUserWithPin', {customer_pin: $scope.credentials.customerPin});
        }
    };
    var postRequest = function(url, data){
        $http.post(baseUrl+url, data).success( function(response) {
            if(response.status === 1){
                switch(url){
                    case 'customerCare/getUserWithPin' : 
                        $scope.getCustomerDetails(response.user);
                        break;
                    case 'customerCare/saveTicket' : 
                        $scope.Save(response);
                        break;
                }                        
            }else{
                $scope.credentials = "";
                alert(response.message);
            }
        }).error( function(){
            console.log('error');
        });
    };
    $scope.Save = function(data){
        if(data){
            window.location = "#/comment";
        }else{
            var keys = ["customerPin", 'ticketId', 'description']
            for(var i in keys){
                if($scope.credentials[keys[i]] == undefined || $scope.credentials[keys[i]] == ""){
                    alert("All fields are mandatory");
                    return;
                }
            }
            $rootScope.ticketData['description'] = $scope.credentials.description;
            postRequest('customerCare/saveTicket', { customer_pin: $scope.credentials.customerPin, ticketId: $scope.credentials.ticketId, isPending: true, reqObject: { description: $scope.credentials.description }});
        }
    };
});
App.controller('ticketCtrl', function($rootScope, $scope, $http, _) {
    $rootScope.requestBtn = false;
    var baseUrl = $rootScope.baseUrl;
    $scope.btns = ["Recent requests", "Resolved requests", "Pending requests"];
    $scope.keys = ['Customer pin', 'Customer Id', 'Ticket Id', 'Request Description', 'Date & time', 'Request state'];
    var postRequest = function(url, data){
        $http.post(baseUrl+url, data).success( function(response) {
            if(response.status === 1){
                switch(url){
                    case 'customerCare/getTickets' : 
                        $scope.allTickets = response._data;
                        break;
                }                        
            }else{
                alert(response.message);
            }
        }).error( function(){
            console.log('error');
        });
    };
    postRequest('customerCare/getTickets');
    $scope.btnClick = function(_name){
        $scope.IsHidden = true;
        switch(_name){
            case $scope.btns[0] :
                $scope.tickets = $scope.allTickets.slice(0, 20);
                break;
            case $scope.btns[1] :
                $scope.tickets = _.chain($scope.allTickets)
                .map(function(v, k){
                    if(v.status == "Resolved")
                        return v;
                })
                .compact()
                .value();
                // $scope.tickets = [];
                break;
            case $scope.btns[2] :
                $scope.tickets = _.chain($scope.allTickets)
                .map(function(v, k){
                    if(v.status == "Pending")
                        return v;
                })
                .compact()
                .value();
                break;
        }
    };
    $scope.onTicketSelect = function (ticket) {
        if(ticket.status == "Pending"){
            $rootScope.ticketData = ticket;
            window.location = '#/comment';
        }
    };
    $scope.back = function() {
        window.location = '#/dashboard';
    };
});
App.controller('commentCtrl', function($rootScope, $scope, $http, _) {
    $rootScope.requestBtn = false;
    $scope.email = $rootScope.ticketData.email;
    $scope.ticketId = $rootScope.ticketData.ticketId;
    var baseUrl = $rootScope.baseUrl;
    $scope.close = function(isPending){
        if($scope.remarks == undefined || $scope.remarks == ""){
            alert("Please enter remarks");
            return ;
        }
        postRequest('customerCare/closeTicket', { customer_pin: $rootScope.ticketData.customer_pin, ticketId: $rootScope.ticketData.ticketId, email: $scope.email, isPending: isPending, reqObject: { remarks: $scope.remarks, status: (isPending ? "Pending" : "Resolved") }});
    };
    $scope.importData = function(){
        // postRequest('hub/getHubs', {user_id: $rootScope.ticketData.user_id, origin_id: 2});
        postRequest('hub/getAllUserDetails', {user_id: $rootScope.ticketData.user_id, origin_id: 2});
    };
    var postRequest = function(url, data){
        $http.post(baseUrl+url, data).success( function(response) {
            if(response.status === 1){
                switch(url){
                    case 'customerCare/closeTicket' : 
                        $rootScope.ticketData = {};
                        window.location = "#/dashboard";
                        break;
                    case 'hub/getAllUserDetails' :
                        $rootScope.customerData = response.userData;
                        window.location = "#/userInfo";
                        break;
                }                        
            }else{
                alert(response.message);
            }
        }).error( function(){
            console.log('error');
        });
    };
});
App.controller('userDataCtrl', function($rootScope, $scope, $http, _) {
    $scope.email = $rootScope.ticketData.email;
    $scope.ticketId = $rootScope.ticketData.ticketId;
    var baseUrl = $rootScope.baseUrl;
    $scope.hubs = _.map($rootScope.customerData.hubDefs,function(v, k){ return {idx: k, device_name: v[0].device_name } });
    $scope.hubClick = function(idx){
        $scope.IsHidden = true;
        $rootScope.hubIdx = idx;
        $scope.keys = _.map($rootScope.customerData.rooms[idx],function(v, k){ return v.room_name });
        $scope.hubKeys = ["Hub name", "Software version", "Software installed date", "Remaining warranty period (in days)"];
        $scope.hubData = [$rootScope.customerData.hubDefs[idx][0].device_name, $rootScope.customerData.hubs[idx].software_version, $rootScope.customerData.hubs[idx].software_install_date];
        var x = new Date(($rootScope.customerData.hubs[idx].hub_install_date.split("T"))[0]);
        x.setDate(x.getDate() + 365);
        // x = ("00" + x.getDate()).slice(-2) + " - " + ("00" + (x.getMonth()+1)).slice(-2) + " - " + x.getFullYear();
        $scope.hubData.push(daysBetween(new Date(), x));
        var obj = {};
        _.map($rootScope.customerData.devices[idx], function(v, k){
            obj[v.room_name] = obj[v.room_name] || [];
            obj[v.room_name].push({ device_name: v.device_name, id: v.device_b_one_id });
        });
        var lengths = _.map($scope.keys, function(v, k){
            if(obj[v] && obj[v].length){
                return obj[v].length;
            }else{
                return 0;
            }
        });
        lengths = Math.max.apply(null, lengths);
        $scope.tableData = _.times(lengths, function(i){
            return _.map($scope.keys, function(v, k){
                if(obj[v] && obj[v][i]){
                    return obj[v][i]
                }else{
                    return {device_name: " ", id: null };
                }
            });
        });
    };
    var daysBetween = function( date1, date2 ) {
        //Get 1 day in milliseconds
        var one_day=1000*60*60*24;

        // Convert both dates to milliseconds
        var date1_ms = date1.getTime();
        var date2_ms = date2.getTime();

        // Calculate the difference in milliseconds
        var difference_ms = date2_ms - date1_ms;
        
        // Convert back to days and return
        difference_ms = Math.round(difference_ms/one_day);
        return (difference_ms > 0 ? difference_ms : 0);
    }
    $scope.onDeviceSelect = function(device_b_one_id){
        if(device_b_one_id){
            $rootScope.device_b_one_id = device_b_one_id;
            window.location = "#/deviceInfo";
        }
    };
    $scope.addRemarks = function(){
        $rootScope.customerData = {};
        window.location = '#/comment';
    };
});
App.controller('deviceInfoCtrl', function($rootScope, $scope, $http, _, $interval) {
    $scope.email = $rootScope.ticketData.email;
    $scope.ticketId = $rootScope.ticketData.ticketId;
    var baseUrl = $rootScope.baseUrl;
    var idx = $rootScope.hubIdx;
    $scope.hubs = [$rootScope.customerData.hubDefs[idx][0].device_name];
    _.map($rootScope.customerData.devices[idx], function(v, k){
        if(v.device_b_one_id == $rootScope.device_b_one_id){
            $scope.hubs.push(v.device_name);
        }
    });
    $scope.moveBack = function(){
        window.location = '#/userInfo';
    };
    $scope.addRemarks = function(){
        $rootScope.customerData = {};
        window.location = '#/comment';
    };
    var postRequest = function(url, data){
        $http.post(baseUrl+url, data).success( function(response) {
            if(response.status === 1){
                switch(url){
                    case 'event/getAllEvents' :
                        $rootScope.eventsData = response.data;
                        $scope.keys1 = [];
                        if($rootScope.eventsData.length !== 0){
                            $scope.keys1 = Object.keys(response.data[0].eventObj);
                            $scope.tableData1 = _.map($rootScope.eventsData,function(v, k){ 
                                                    return _.map($scope.keys1, function(val){
                                                        return v.eventObj[val];
                                                    });
                                                });
                        }
                        break;
                    case 'event/getOldStatus' : 
                        $rootScope.statusData = response.data;
                        $scope.keys = Object.keys(response.data[0]);
                        _.map(['modified', '_id', 'hub_id', 'user_id', 'device_b_one_id', 'device_id'], function(v, k){
                            $scope.keys.splice($scope.keys.indexOf(v), 1)
                        });
                        $scope.tableData = _.map($rootScope.statusData,function(v, k){ 
                                                return _.map($scope.keys, function(val){
                                                    return v[val];
                                                });
                                            });
                        break;
                }                        
            }else{
                alert(response.message);
            }
        }).error( function(){
            console.log('error');
        });
    };
    var refreshEvents = function () {
        postRequest("event/getAllEvents", { hub_id: $rootScope.customerData.hubDefs[idx][0].hub_id, device_b_one_id: $rootScope.device_b_one_id, count: 10, origin_id: 2 });
        postRequest("event/getOldStatus", { hub_id: $rootScope.customerData.hubDefs[idx][0].hub_id, device_b_one_id: $rootScope.device_b_one_id, count: 10, origin_id: 2 });
    };
    refreshEvents();
    $interval(refreshEvents, 1000);
});