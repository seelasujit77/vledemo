var Path = require('path');

function Globals() {
};

//DB configurations
Globals.prototype.MongoPort = 27017;
//Current production server
Globals.prototype.MongoHost = '172.31.12.148'; // '172.31.13.247';
Globals.prototype.MongoDB = 'vledb';

Globals.prototype.MailLogo = "/images/mail_logo.png";
Globals.prototype.appRoot = Path.resolve(__dirname);
Globals.prototype.maxContacts = 8;
Globals.prototype.maxBOneIdChars = 4;

Globals.prototype.LoggerConfig = {
    appenders: [
        {type: 'console'},
        {
            "type": "file",
            "filename": Globals.prototype.appRoot + "/logs/log_file.log",
            "maxLogSize": 10485760,
            "backups": 100,
            "category": "relative-logger"
        }
    ]
};


module.exports = new Globals();
